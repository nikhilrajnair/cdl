import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import * as L from "leaflet";
import { Layer, tileLayer, geoJSON, LayerOptions } from 'leaflet';
import 'leaflet-providers';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // constructor() { }

  ngOnInit(): void {
    this.abc();
  }

  dataSource: any;
  displayedColumns: string[] = ['ParliamentaryConstituency', 'GrandTotalExpenditure', 'OpeningBalance', 'TotalExpenditureonMaterials', 'TotalExpenditureonWages', 'TotalFundAvailable', 'TotalPaymentDue', 'TotalUnspentBalance'];

  map: L.Map;
  layers: Layer[];
  dataPoint: any = [];
  options = {
    layers: [
      L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 18,
        attribution: ""
      })
    ],
    zoom: 7,
    center: L.latLng(20.9517, 85.0985),
    // center : [59.9386300, 30.3141300],
    fitBounds: [[60.2448369, 29.6998985], [59.6337832, 30.254172]],

  };

  constructor(private http: HttpClient) { }




  abc() {
    this.http.get('assets/data_mgnregs_odisha_2019-20_pc_expenditure.csv', { responseType: 'text' })
      .subscribe(
        data => {
          // console.log(data);
          let mappedJsonData = [];
          let lines = data.split("\n");
          let header = lines[0].split(",");

          for (let i = 1; i < lines.length - 1; i++) {
            let element = lines[i];
            let eacheElement = element.split(",");
            // console.log(eacheElement);
            let el: any = {};
            header.map((keys: any, index: number) => {
              el[keys] = eacheElement[index];
            })
            // console.log(el);
            mappedJsonData.push(el);
            // console.log(lines[i])
          }
          mappedJsonData = JSON.parse(JSON.stringify(mappedJsonData).replace(/\s/g, ''));
          console.log(mappedJsonData);
          this.dataSource = new MatTableDataSource<any>(mappedJsonData);
          // this.dataSource.paginator = this.paginator;
        },
        error => {
          console.log(error);
        }
      );
  }

  onMapReady(map: L.Map) {
    this.http.get("assets/departments.json").subscribe((data: any) => {
      console.log(data);
      this.dataPoint = data;
      console.log(typeof this.dataPoint)
      this.dataPoint = this.dataPoint.features.filter(x => x.properties.st_name == "Orissa");
      console.log(this.dataPoint);
      // L.geoJSON(this.json).addTo(map);
      L.geoJSON(this.dataPoint, {
        onEachFeature: function (feature, layer,) {
          // console.log(feature);
          layer.bindPopup('<h1>' + feature.properties.pc_name + '</h1><p>name: ' + feature.properties.pc_name_hi + '</p>');
        }
      }).addTo(map);

    });
  }

}


